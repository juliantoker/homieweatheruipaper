    var canvas = document.getElementById('myCanvas');
    var scale = window.devicePixelRatio; // Change to 1 on retina screens to see blurry canvas.
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    // console.log('scale: ' + scale + " canvasWidth: " + canvas.width + " canvasHeight: " + canvas.height + " canvasStyleWidth: " + canvas.style.width + "canvasStyleHeight: " + canvas.style.height);
    // console.log('windowWidth: ' + window.screen.width + 'windowHeight: ' + window.screen.height + ' availWidth: ' + window.innerWidth + ' availHeight: ' + window.innerHeight);
    var temp = 38;
    var hiTemp = 39;
    var lowTemp = 34;
    var activeTimers = [];

    var weatherStatus = 'Cloudy';

    var col;
    var barCol;

    var bgColor = '#f2a444';
    var barColor = '#f27406';

    var coldBGColor = '#99b8bf';
    var coldBarColor = '#3b414a';

    var briskBGColor = '#a194ff';
    var briskBarColor = '#514a80';

    var moderateBGColor = '#23bf9e';
    var moderateBarColor = '#0c4035';

    var warmBGColor = '#f9cc3a';
    var warmBarColor = '#7a641c';

    var warmerBGColor = '#f2a444';
    var warmerBarColor = '#f27406';

    var hotBGColor = '#f93614';
    var hotBarColor = '#7a1a0a';

    var barSize = 0.90;
    var clockPadding = 20;

    var freedomHater = false;

    var rayGroup = new Group();
    var faceGroup = new Group();
    var faceAndRayGroup = new Group(rayGroup,faceGroup);
    var baseCloudGroup = new Group({name: 'baseCloud'});
    var homieGroup = new Group();
    var tempGroup = new Group();
    var baseTimerGroup = new Group();
    var timerGroup = new Group();
    var bottomBar;

    DrawBackgroundBox();
    //ConfigureHomieGroup();
    //DrawHomieFace_SunFaceCenter();
    ConfigureCloud();
    ConfigureCurrentTemperature();
    ConfigureWeatherStatusText();
    DrawBottomBar();
    ConfigureTimerGroup();
    DrawClock();
    ConfigureHomieGroup();
    // homieGroup.selected = true;
    // tempGroup.selected = true;

    function DrawBackgroundBox() {
        if(temp < 33) {
            col = coldBGColor;
            barCol = coldBarColor;
        } else if(temp < 50) {
            col = briskBGColor;
            barCol = briskBarColor;
        } else if(temp < 60) {
            col = moderateBGColor;
            barCol = moderateBarColor;
        } else if(temp < 75) {
            col = warmBGColor;
            barCol = warmBarColor;
        } else if (temp < 90) {
            col = warmerBGColor;
            barCol = warmerBarColor;
        } else {
            col = hotBGColor;
            barCol = hotBarColor;
        }
        var rectangle = new Rectangle(new Point(0, 0), new Size(window.innerWidth, window.innerHeight));
        var shape = new Shape.Rectangle(rectangle);
        shape.fillColor = col;
        shape.sendToBack();
    }

    function ConfigureHomieGroup() {
        homieGroup.position = view.center;
        //faceAndRayGroup.scale(0.975);
        homieGroup.position.x += window.screen.availWidth/6;
        homieGroup.position.y -= window.screen.availHeight/10;
    }

    function DrawHomieFace(point,faceRadius) {
        //Face drawing must be adjusted to work with (point) as opposed to face as found in DrawHomieFace_SunFaceCenter()
        //The same holds true for faceRadius replacing (face.radius) as in DrawHomieFace_SunFaceCenter()


    }

    function DrawHomieFace_SunFaceCenter() {
        var face = new Shape.Circle(new Point(window.innerWidth/3, window.innerHeight/3), window.innerHeight * 0.140);
        face.fillColor = "white";
        face.name = 'face';
        var leftEye = new Shape.Circle(new Point(face.position.x - face.radius/2.35, face.position.y - face.radius/2.45), face.radius * 0.102094240837696);
        var rightEye = new Shape.Circle(new Point(face.position.x + face.radius/2.35, face.position.y - face.radius/2.45), face.radius * 0.102094240837696);

        leftEye.fillColor =  col;
        leftEye.name = 'leftEye';

        rightEye.fillColor = col;
        rightEye.name = 'rightEye'

        var leftMouthPoint = new Point(leftEye.position.x, face.position.y - face.radius/8);
        var middleMouthPoint = new Point(face.position.x, face.position.y);
        var rightMouthPoint = new Point(rightEye.position.x, face.position.y - face.radius/8);

        var myArc = new Path.Arc(leftMouthPoint,middleMouthPoint,rightMouthPoint);
        myArc.strokeColor = col;
        myArc.strokeWidth = 10;
        myArc.strokeCap = 'round';
        myArc.strokeJoin = 'round';

        faceGroup = new Group([face,leftEye,rightEye,myArc]);
        faceGroup.position = view.center;

        DrawHomieFace_SunRays(faceGroup.position, face.radius);
        homieGroup.addChild(faceGroup);
        homieGroup.addChild(rayGroup);
        
        homieGroup.scale(1.25);
    }

    function DrawHomieFace_SunRays(faceCenter, faceRadius) {
        var rectangle = new Rectangle(faceCenter, new Size(faceRadius/5, faceRadius/3));
        rectangle.center = faceCenter;
        var cornerSize = new Size(faceRadius/10, faceRadius/10);
        var path = new Path.Rectangle(rectangle, cornerSize);
    
        path.fillColor = 'white';
        path.strokeJoin = 'round';
1
        path.position.y += faceRadius + path.bounds.height;
        rayGroup.addChild(path);
        for(var i = 1; i < 8; i++) { //Can start from zero if we do the trick for the base ray but it's not needed
            var p = path.clone();
            p.rotate(45 * i, faceCenter);
            rayGroup.addChild(p);
        }
        //path.rotate(45, faceCenter);
    }

    function ConfigureCloud() {
        var biggerCircle = new Path.Circle({
            center: view.center,
            radius: window.innerHeight/5,
            fillColor: col,
            strokeWidth: 12,
            strokeJoin: 'round',
            strokeCap: 'round',
            strokeColor: 'white',
            selected: false
        });

        var smallerCircle = biggerCircle.clone();
        smallerCircle.position.x += biggerCircle.bounds.width/1.5;
        smallerCircle.position.y += biggerCircle.bounds.height/2 - (0.4 * biggerCircle.bounds.height);
        smallerCircle.scale(0.8);
        
        
        var unitedCircles = biggerCircle.unite(smallerCircle);
        unitedCircles.removeSegment(6);
        unitedCircles.selected = false;

        var spos = unitedCircles.segments[2].point;
        var acc = new Path.Arc({
            from: unitedCircles.segments[3].point,
            through: spos,
            to: new Point(smallerCircle.segments[0].point.x + 15, smallerCircle.segments[0].point.y - 60), //Will want to turn 15,60 to be in terms of something else
            strokeColor: 'white',
            strokeWidth: 12,
            strokeJoin: 'round',
            strokeCap: 'round',
            selected: false
        });
        biggerCircle.remove(); //Deleting the geometry we used for contruction so it doesn't clutter
        smallerCircle.remove();
        
        
        baseCloudGroup.addChild(unitedCircles); //Adding construction to baseCloud group so that it can be reused again and again
        baseCloudGroup.addChild(acc);
        baseCloudGroup.bringToFront();
        baseCloudGroup.position = homieGroup.position;
        baseCloudGroup.position.x += 0;
        baseCloudGroup.position.y += 0;
        homieGroup.addChild(baseCloudGroup);
        
    }

    function ConfigureCurrentTemperature() {
        var pp = new Point(window.innerWidth/8, window.innerHeight/8);

        var tempText = new PointText(pp);
        tempText.justification = 'center';
        tempText.fontFamily = 'Rubik';
        tempText.fillColor = 'white';
        tempText.content = temp;
        tempText.fontSize = window.innerWidth/100 + 'em';
        tempText.name = 'tempText';
        // tempText.selected = true;
        //tempText.center = tempText.position;
        tempGroup.addChild(tempText);

        ConfigureTemperatureUnits();
        tempGroup.position = new Point(window.innerWidth/5 , window.innerHeight/4 );
        console.log(tempGroup.center);
    }

    function ConfigureTemperatureUnits() {
        var unitTextContent;
        if(!freedomHater) {
            unitTextContent = "F";
        } else {
            unitTextContent = "C";
        }

        var degreeText = new PointText(tempGroup.position);
        degreeText.justification = 'center';
        degreeText.fillColor = 'white';
        degreeText.fontFamily = 'Rubik';
        degreeText.content = 'o';
        degreeText.fontSize = '5em';
        degreeText.name = 'degreeText';
        degreeText.position = new Point(tempGroup.position.x + tempGroup.bounds.width/2,
                                    (tempGroup.position.y - tempGroup.bounds.height/2 
                                        + degreeText.bounds.height/2));
        tempGroup.addChild(degreeText);

        var unitText = new PointText();
        unitText.justification = 'center';
        unitText.fillColor = 'white';
        unitText.fontFamily = 'Rubik';
        unitText.content = unitTextContent;
        unitText.fontSize = '10em';
        unitText.name = 'unitText';
        unitText.position = new Point(degreeText.position.x + degreeText.bounds.width/2 + unitText.bounds.width/2,
                                     degreeText.position.y + degreeText.bounds.height/2);
        tempGroup.addChild(unitText);

        var hiLoGroup = new Group({
            position: tempGroup.position
        });

        var hiText = new PointText({
            justification: 'center',
            fillColor: 'white',
            fontFamily: 'Rubik',
            content: hiTemp,
            fontSize: '2.25em',
            name: 'hiText',
            // point: new Point(tempGroup.position.x + tempGroup.firstChild.bounds.width/4,
            //                          tempGroup.position.y + tempGroup.firstChild.bounds.height/4)
            point: hiLoGroup.position
        });
        hiLoGroup.addChild(hiText);

        var separatorText = new PointText({
            justification: 'center',
            fillColor: 'white',
            fontFamily: 'Rubik',
            content: '/',
            fontSize: '2.25em',
            name: 'separatorText',
            point: new Point(hiText.position.x + hiText.bounds.width/1.5,
                                     hiText.position.y + hiText.bounds.height/2)
        });
        hiLoGroup.addChild(separatorText);

        var lowText = hiText.clone();
        lowText.content = lowTemp;
        lowText.name = 'lowText';
        lowText.position = new Point(separatorText.position.x + lowText.bounds.width/2,
                                separatorText.position.y + lowText.bounds.height/2);
        hiLoGroup.addChild(lowText);

        hiLoGroup.position = new Point(degreeText.position.x + degreeText.bounds.width/1.5, tempGroup.position.y + tempGroup.bounds.height/4);
        tempGroup.addChild(hiLoGroup);
    }

    function ConfigureWeatherStatusText() {
        var statusText = new PointText({
            point: new Point(homieGroup.position.x, homieGroup.position.y + homieGroup.bounds.height/1.5),
            content: weatherStatus,
            fillColor: 'white',
            fontFamily: 'Rubik',
            fontSize: '2.5em',
            justification: 'center',
            name: 'statusText'
        });

        homieGroup.addChild(statusText);
    }

    function DrawBottomBar() {
        bottomBar = new Shape.Rectangle({
            point: new Point(0, window.innerHeight - (1 - barSize) * window.innerHeight),
            size: new Size(window.innerWidth, (1 - barSize) * window.innerHeight),
            fillColor: barCol,
            blendMode: 'normal'
        });
    }

    function DrawClock() {
        new PointText({
            point: new Point(window.innerWidth - clockPadding, 36 + clockPadding),
            fillColor: 'white',
            fontFamily: 'Rubik',
            fontSize: '3em',
            justification: 'right',
            content: '4:20 AM',
            selected: false
        });
    }

    function ConfigureTimerGroup() {
        timerGroup.pivot = bottomBar.point;
        timerGroup.bringToFront();
        ConfigureBaseTimer();
        timerGroup.position.y += baseTimerGroup.bounds.height/4;
        
    }

    function ConfigureBaseTimer() {

        //CONFIGURE POSITION AND LAYERING OF BASE TIMER CONTAINER

        baseTimerGroup.pivot = new Point(0, barSize * window.innerHeight);
        baseTimerGroup.bringToFront();


        //CREATING THE BACKGROUND CONTAINER FOR BASE TIMER

        var baseTimerSize = new Size(window.innerWidth/8,bottomBar.size.height/1.5);
        var timer = new Path.Rectangle({
            size: baseTimerSize,
            //position: baseTimerGroup.position + new Point(baseTimerSize.width/1.80, baseTimerSize.height/2 + (bottomBar.size.height - baseTimerSize.height)/8),
            topLeft: bottomBar.point,
            radius: baseTimerSize.width/6,
            fillColor: col,
            name: 'timerBG',
            selected: false
        });
        //baseTimerGroup.pivot = timer.position;
        baseTimerGroup.addChild(timer);


        //ADDING TIME AND NAME TEXT COMPONENTS TO BASE TIMER

        var timeText = new PointText({
            point: timer.position,
            fillColor: 'white',
            fontFamily: 'Rubik',
            fontSize: '1.5em',
            justification: 'center',
            content: '00:00:00',
            name: 'timeText'
        });

        var nameText = new PointText({
            point: new Point(timer.position.x, timer.position.y + timer.bounds.height/4),
            fillColor: 'white',
            fontFamily: 'Rubik',
            fontSize: '1em',
            justification: 'center',
            content: 'Laundry',
            name: 'nameText'
        });
        baseTimerGroup.addChild(timeText);
        baseTimerGroup.addChild(nameText);

        baseTimerGroup.visible = false;


        //TESTING MULTIPLE TIMERS HERE WILL BE ROLLED INTO DRAWACTIVETIMERS() EVENTUALLY

        for(var i = 0; i < 5; i++) {
            var newTimer = baseTimerGroup.clone();
            newTimer.visible = true;
            newTimer.position.x += (i * newTimer.bounds.width) + i * newTimer.bounds.width/12;
            newTimer.name = "timer: " + i;
            timerGroup.addChild(newTimer);
        }
        
    }

    function DrawActiveTimers(timerArray) {
        var usingRealData = false;
        if( usingRealData ) {
            //Do the stuff...
        } else {
            //Create dummy timers...

        }
    }

    function CreateTimer(name,timerLength) {

    }

    function UpdateTimers() {

    }

    function DeleteTimer() {

    }

    function DrawWeatherIcon() {
        if(weatherStatus == 'Cloudy') {

        }
    }

    function SetWeatherStatus(newWeatherStatus) {
        weatherStatus = newWeatherStatus;
    }

    //ADD TIMER OBJECT HERE THESE WILL BE THE OBJECTS THAT BIND THE TIMER/NAME DATA

    function onFrame(event) {
        rayGroup.rotate(0.5, faceGroup.center);
    }
